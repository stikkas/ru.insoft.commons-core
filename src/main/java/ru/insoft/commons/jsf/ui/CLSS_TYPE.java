package ru.insoft.commons.jsf.ui;

public enum CLSS_TYPE {

	/**
	 * выбор значений строго из справочника
	 */
	strict,
	/**
	 * помимо STRICT допустим ввод своего значения
	 */
	soft,
	/**
	 * помимо SOFT введенное значение добавляется к справочнику
	 */
	open;

}
