package ru.insoft.commons.jsf.ui;

/**
 * 
 * @author val
 * 
 */
public interface GetPaginator {

	public Paginator getPaginator();

}
