package ru.insoft.commons.jsf.ui;

/**
 * 
 * @author val
 * 
 */
public interface GetHistory<T> {

	public History<T> getHistory();

}
