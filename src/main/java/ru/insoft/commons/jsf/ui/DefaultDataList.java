package ru.insoft.commons.jsf.ui;

import java.util.Collection;

/**
 * 
 * @author val
 * 
 */
public interface DefaultDataList<T, S extends SearchCriteria, C extends SortingCriteria, DM extends Iterable<T>, UI>
		extends SelectionDataListSingle<T, S, C, DM, UI> {

	/**
	 * get column name of id
	 * 
	 * @return
	 */
	String getIdColumnName();

	/**
	 * get list of column name
	 * 
	 * @return
	 */
	Collection<String> getColumnsName();

	/**
	 * get value property of the object
	 * 
	 * @param object
	 * @param property
	 * @return
	 */
	Object getValue(T object, String property);

	/**
	 * get id property of the object
	 * 
	 * @param object
	 * @return
	 */
	Object getId(T object);
}
