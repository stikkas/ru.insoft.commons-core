package ru.insoft.commons.jsf.ui;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author val
 * 
 */
public interface SimpleSortingCriteria extends SortingCriteria {

	/**
	 * return the map[column,order], order = org.richfaces.component.SortOrder
	 * 
	 * @return
	 */
	Map<String, Object> getSortingMap();

	// SortOrder valueOf(String sortOrder);
	//
	/**
	 * return the list of columns by priority.<br/>
	 * Must return the all columns involved in the sorting (the all keys of
	 * getSortingMap() map).
	 * 
	 * @return
	 */
	List<String> getPriorityList();

}
