package ru.insoft.commons.jsf.ui;

import com.google.common.base.Objects;
import com.google.common.base.Strings;

public enum STRING_COMPARE_TYPE {

	/**
	 * equals
	 */
	equals,
	/**
	 * start with
	 */
	startWith,
	/**
	 * include
	 */
	include,
	/**
	 * equals with ignore case
	 */
	equalsIC,
	/**
	 * start with ignore case
	 */
	startWithIC,
	/**
	 * include ignore case
	 */
	includeIC;

	public static boolean compare(STRING_COMPARE_TYPE compareType,
			String value, String filter) {
		switch (compareType) {
		case equals:
			return Objects.equal(value, filter);
		case equalsIC:
			return Objects.equal(Strings.nullToEmpty(value).toLowerCase(),
					Strings.nullToEmpty(filter).toLowerCase());
		case include:
			return Strings.nullToEmpty(value).indexOf(filter) >= 0;
		case includeIC:
			return Strings.nullToEmpty(value).toLowerCase()
					.indexOf(Strings.nullToEmpty(filter).toLowerCase()) >= 0;
		case startWith:
			return Strings.nullToEmpty(value).startsWith(filter);
		case startWithIC:
			return Strings.nullToEmpty(value).toLowerCase()
					.startsWith(Strings.nullToEmpty(filter).toLowerCase());
		}
		return false;
	}

}
