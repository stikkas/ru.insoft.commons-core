package ru.insoft.commons.jsf.ui;

import java.util.List;

/**
 * 
 * @author val
 * 
 */
public interface SelectionDataList<T, C extends SearchCriteria, O extends SortingCriteria, DM extends Iterable<T>, UI>
		extends DataList<T, C, O, DM, UI> {

	/**
	 * get list of selection rowKey
	 * 
	 * @return
	 */
	List<Object> getSelectionRowKeyList();

	/**
	 * get list of selection object
	 * 
	 * @return
	 */
	List<T> getSelectionList();

}
