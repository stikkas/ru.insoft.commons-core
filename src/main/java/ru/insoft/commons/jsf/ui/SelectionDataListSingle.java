package ru.insoft.commons.jsf.ui;

/**
 * 
 * @author val
 * 
 */
public interface SelectionDataListSingle<T, S extends SearchCriteria, O extends SortingCriteria, DM extends Iterable<T>, UI>
		extends DataList<T, S, O, DM, UI> {

	/**
	 * get selection rowKey
	 * 
	 * @return
	 */
	Object getSelectionRowKey();

	void setSelectionRowKey(Object rowKey);

	/**
	 * get selection object
	 * 
	 * @return
	 */
	T getSelection();

}
