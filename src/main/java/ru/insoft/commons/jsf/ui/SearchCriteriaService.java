//package ru.insoft.commons.jsf.ui;
//
//import java.util.Collection;
//import java.util.List;
//
//import ru.insoft.commons.jsf.ui.SearchCriteria;
//import ru.insoft.commons.jsf.ui.SortingCriteria;
//
//public interface SearchCriteriaService<E, S extends SearchCriteria, C extends SortingCriteria> /*
//																								 * extends
//																								 * SearchCriteria
//																								 */{
//
//	String getIdColumnName();
//
//	Class getIdColumnJavaType();
//
//	Collection<String> getColumnsName();
//
//	List<E> getPage4SearchService(S searchCriteria, C sortingCriteria,
//			int firstResult, int maxResults);
//
//	int getCount4SearchService(S searchCriteria);
//
//	E getEntityById(String id);
//
//}
