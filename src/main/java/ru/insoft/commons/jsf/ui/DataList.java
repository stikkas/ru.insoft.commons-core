package ru.insoft.commons.jsf.ui;

import java.io.Serializable;

/**
 * 
 * @author val
 * 
 */
public interface DataList<T, C extends SearchCriteria,  O extends SortingCriteria, DM extends Iterable<T>, UI>
		extends Serializable,
		GetDataModel<T, C, O, SearchService<T, C, O>, DM>,
		GetSearchCriteria<C>, 
		GetSortingCriteria<O>,
		GetSearchService<T, C, O, SearchService<T, C, O>>, CustomUI<UI> {

	/**
	 * toggle sorting
	 */
	void toggleSort();

	/**
	 * toggle filtering
	 */
	void toggleFilter();

	/**
	 * reset dataList
	 */
	void reset();
}
