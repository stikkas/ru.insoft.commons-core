package ru.insoft.commons.jsf.ui;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 
 * @author val
 * 
 */
public interface SearchService<T, C extends SearchCriteria, O extends SortingCriteria>
		extends Serializable {

	List<T> getPage(C searchCriteria, O sortingCriteria, int firstResult,
			int maxResults);

	int getCount(C searchCriteria);

	String getIdColumnName();

    List<T> getContent(C searchCriteria, O sortingCriteria);

	Class getIdColumnJavaType();

	Collection<String> getColumnsName();

	// List<E> getPage4SearchService(S searchCriteria, C sortingCriteria,
	// int firstResult, int maxResults);

	// int getCount4SearchService(S searchCriteria);

	T getEntityById(String id);

}
