package ru.insoft.commons.jsf.ui;

/**
 * for binding property
 * 
 * @author val
 * 
 */
public interface CustomUI<UI> {

	UI getUi();

	void setUi(UI ui);
}
