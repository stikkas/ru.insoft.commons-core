package ru.insoft.commons.jsf.ui;

import java.util.Collection;

public interface History<T> {

	Collection<T> getHistoryList();

	void clear();

	void push(T t);

	void backward();

	void forward();

	/**
	 * 
	 * @return -1 - is last
	 */
	int getHistoryIndex();

	T getCurrent();

	boolean isExistBackward();

	boolean isExistForward();

	boolean isEmpty();

}
