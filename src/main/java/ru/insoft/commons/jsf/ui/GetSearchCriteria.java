package ru.insoft.commons.jsf.ui;

/**
 * 
 * @author val
 * 
 */
public interface GetSearchCriteria<C extends SearchCriteria> {

	C getSearchCriteria();

}
