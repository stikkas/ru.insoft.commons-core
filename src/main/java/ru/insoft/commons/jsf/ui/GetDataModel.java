package ru.insoft.commons.jsf.ui;

/**
 * 
 * @author val
 * 
 */
public interface GetDataModel<T, C extends SearchCriteria, O extends SortingCriteria, S extends SearchService<T, C, O>, DM extends Iterable<T>> {

	DM getModel();
}
