package ru.insoft.commons.jsf.ui;

import java.util.Map;

/**
 * 
 * @author val
 * 
 */
public interface SimpleSearchCriteria extends SearchCriteria {
	/**
	 * return the map[column,filterValue]
	 * 
	 * @return
	 */
	Map<String, String> getFilterMap();

	/**
	 * return the map[column,compareType]
	 * 
	 * @return
	 */
	Map<String, STRING_COMPARE_TYPE> getCompareTypeMap();

	// /**
	// * return the string of "where"
	// *
	// * @return field1 like 'filterValue1%', field2 like 'filterValue2%'...
	// */
	// String getStringWhere();
	//
	// /**
	// * transform query string with filter
	// *
	// * @param query
	// * @return select * from (query) where ...
	// */
	// String whereTransform(String query);

}
