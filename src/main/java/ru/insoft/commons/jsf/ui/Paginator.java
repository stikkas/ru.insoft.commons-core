package ru.insoft.commons.jsf.ui;

/**
 * 
 * @author val
 * 
 */
public interface Paginator {

	/**
	 * next page
	 */
	public void next();

	/**
	 * previous page
	 */
	public void previous();

	/**
	 * first page
	 */
	public void first();

	/**
	 * last page
	 */
	public void last();

	/**
	 * is next page available?
	 * 
	 * @return
	 */
	public boolean isNextAvailable();

	/**
	 * is previous page available?
	 * 
	 * @return
	 */
	public boolean isPreviousAvailable();

	/**
	 * get first index of element current page
	 * 
	 * @return
	 */
	public long getStart();

	/**
	 * get end index of element current page
	 * 
	 * @return
	 */
	public long getEnd();

	/**
	 * get offset record - begin index on the current page
	 * 
	 * @return
	 */
	public int getOffset();

	/**
	 * get total page
	 * 
	 * @return
	 */
	public int getTotalPage();

	/**
	 * get current page (numeration from 0)
	 * 
	 * @return
	 */
	public int getPage();

	/**
	 * set page (numeration from 0)
	 * 
	 * @param page
	 */
	public void setPage(int page);

	/**
	 * get current page (numeration from 1)
	 * 
	 * @return
	 */
	public int getRealPage();

	/**
	 * set page (numeration from 1)
	 * 
	 * @param page
	 */
	public void setRealPage(int page);

	/**
	 * get default count of record on the page
	 * 
	 * @return
	 */
	public int getPageSize();

	public void setPageSize(int pageSize);

	/**
	 * get count of records on the current page
	 * 
	 * @return
	 */
	public long getRecordCount();

	/**
	 * get all count of records
	 * 
	 * @return
	 */
	public long getTotalRecordCount();

}
