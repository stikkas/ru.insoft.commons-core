package ru.insoft.commons.jsf.ui;

/**
 * 
 * @author val
 * 
 */
public interface SimpleDataList<T, DM extends Iterable<T>, UI> extends
		DefaultDataList<T, SimpleSearchCriteria, SimpleSortingCriteria, DM, UI> {

}
