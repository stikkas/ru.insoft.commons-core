package ru.insoft.commons.jsf.ui;

import java.io.Serializable;

/**
 * 
 * @author val
 * 
 */
public interface Criteria extends Serializable {
	/**
	 * reset criteria
	 */
	public void reset();

	/**
	 * search criteria is empty
	 * 
	 * @return
	 */
	public boolean isEmpty();

}
