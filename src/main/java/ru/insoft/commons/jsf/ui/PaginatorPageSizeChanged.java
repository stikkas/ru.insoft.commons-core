package ru.insoft.commons.jsf.ui;

import java.util.EventObject;

/**
 * 
 * @author val
 * 
 */
public interface PaginatorPageSizeChanged<EO extends EventObject> extends
		Paginator {

	/**
	 * listener for pageSize changed
	 * 
	 * @param event
	 */
	public void pageSizeChanged(EO event);

}
