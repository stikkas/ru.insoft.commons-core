package ru.insoft.commons.jsf.ui;

/**
 * 
 * @author val
 * 
 */
public interface GetSortingCriteria<O extends SortingCriteria> {

	public O getSortingCriteria();

}
