package ru.insoft.commons.jsf.ui;

/**
 * 
 * @author val
 * 
 */
public interface GetSearchService<T, C extends SearchCriteria, O extends SortingCriteria, S extends SearchService<T, C, O>> {

	S getSearchService();

}
