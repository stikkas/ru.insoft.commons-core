package ru.insoft.commons.hibernate;

/**
 * 
 * @author val
 * 
 * @param <T>
 */
public interface GetId<ID> {

	ID getId();

}
