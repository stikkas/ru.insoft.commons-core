package ru.insoft.commons.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.io.ByteStreams;
import com.google.common.io.Closeables;

public final class ZipUtils {

	public static void unzip(File zipFile, File outDir)
			throws FileNotFoundException, IOException {
		ZipInputStream in = null;
		try {
			in = new ZipInputStream(new BufferedInputStream(
					new FileInputStream(zipFile)));
			ZipEntry entry;
			while ((entry = in.getNextEntry()) != null) {
				File file = new File(outDir, entry.getName());
				// System.out.println(file);
				if (!file.exists()) {
					if (entry.isDirectory()) {
						boolean b = file.mkdir();
						if (!b) {
							throw new IOException(
									"Could not create directory : "
											+ file.getParentFile());
						}
					} else {
						boolean b = false;
						try {
							b = file.createNewFile();
							if (b) {
								BufferedOutputStream out = new BufferedOutputStream(
										new FileOutputStream(file));
								ByteStreams.copy(in, out);
								out.flush();
								out.close();
							}
						} catch (IOException e) {
							throw new IOException("Could not create file : "
									+ file.getParentFile(), e);
						}
						if (!b) {
							throw new IOException("Could not create file : "
									+ file.getParentFile());
						}
					}
				}
			}
		} finally {
			Closeables.closeQuietly(in);
		}
	}

	public static final String[] unzip(File zipFile)
			throws FileNotFoundException, IOException {
		List<String> list = new ArrayList<String>(0);
		ZipInputStream in = null;
		try {
			in = new ZipInputStream(new BufferedInputStream(
					new FileInputStream(zipFile)));
			ZipEntry entry;
			while ((entry = in.getNextEntry()) != null) {
				list.add(entry.getName());
			}
			return list.toArray(new String[0]);
		} finally {
			Closeables.closeQuietly(in);
		}
	}

	public static final String[] findInZip(final File zipFile,
			final String match) throws FileNotFoundException, IOException {
		String[] files = unzip(zipFile);
		return Collections2.filter(ImmutableList.copyOf(files),
				new Predicate<String>() {
					@Override
					public boolean apply(String input) {
						return input.matches(match);
					}
				}).toArray(new String[] {});
	}

}
