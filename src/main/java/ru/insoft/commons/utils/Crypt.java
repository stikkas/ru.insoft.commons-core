package ru.insoft.commons.utils;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * @author val
 */
public final class Crypt {
	private Crypt() {
	}

	static final byte[] HEX_CHAR_TABLE = { (byte) '0', (byte) '1', (byte) '2',
			(byte) '3', (byte) '4', (byte) '5', (byte) '6', (byte) '7',
			(byte) '8', (byte) '9', (byte) 'a', (byte) 'b', (byte) 'c',
			(byte) 'd', (byte) 'e', (byte) 'f' };

	/**
	 * bytes to hex.
	 * 
	 * @param bytes
	 * @return
	 */
	public static String toHex(final byte[] bytes) {
		byte[] hex = new byte[2 * bytes.length];
		int index = 0;

		for (byte b : bytes) {
			int v = b & 0xFF;
			hex[index++] = HEX_CHAR_TABLE[v >>> 4];
			hex[index++] = HEX_CHAR_TABLE[v & 0xF];
		}
		try {
			return new String(hex, "ASCII");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] getHash(final byte[] bytes, String method)
			throws NoSuchAlgorithmException {
		return java.security.MessageDigest.getInstance(method).digest(bytes);
	}

	/**
	 * generate md5.
	 * 
	 * @param string
	 * @return
	 */
	public static String md5(String string) {
		try {
			return toHex(getHash(string.getBytes("UTF-8"), "MD5"));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * generate sha1.
	 * 
	 * @param string
	 * @return
	 */
	public static String sha1(String string) {
		try {
			return toHex(getHash(string.getBytes("UTF-8"), "SHA1"));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		System.out.println(md5("password"));
		System.out.println(sha1("password"));
	}
}
